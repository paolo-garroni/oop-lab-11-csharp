﻿using System;

namespace Properties {

    public class Card
    {
        public string Seed { get; }
        public string Name { get; }
        public int Ordinal { get; }

        public Card(string name, string seed, int ordial)
        {
            if (name != null || seed != null)
            {
                this.Name = name;
                this.Ordinal = ordial;
                this.Seed = seed;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        public override string ToString()
        {
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordinal})";
        }

        public override bool Equals(object obj)
        {
            if (obj is Card)
            {
                Card card = (Card)obj;
                return this.Name == card.Name && this.Seed == card.Seed && this.Ordinal == card.Ordinal;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Seed.GetHashCode() ^ Ordinal;
        }
    }

}